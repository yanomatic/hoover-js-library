# HooverJS

Hoover JS library for programmable interaction with Hoover washer dryer. Supports HomeBridge & IFTTT.

## Laundry Cycle State

```
  PROGRAM_SELECTION = 1,
  PROGRAM_EXECUTION = 2,
  PROGRAM_PAUSE = 3,
  DELAYED_START_SELECTION = 4,
  DELAYED_START_EXECUTED = 5,
  ERROR = 6,
  PROGRAM_ENDED = 7,
```

## Washer Status Schema

```
  WiFiStatus, // remote control enabled
  Err,
  MachMd, // Cycle state values
  Pr, // Program
  PrPh, // Program phase
  PrCode, // Program code
  SLevel, // soil level
  Temp, // water temperature
  SpinSp, // spin speed value * 1000
  Steam, // steam on/off
  DryT, // dryer on/off
  DelVal, // delayed start
  RemTime, // remaining time
  RecipeId,
  Lang,
  FillR, // fill percentage
  DisTestOn,
  DisTestRes,
  CheckUpState,
  Det, // Detergent (bool)
  Soft, // Softener (bool)
  DetWarn, // Detergent warning (bool)
  SoftWarn,
  DetPreW,
  SoftPreW,
  DPrgCnt,
  SPrgCnt,
  WaterHard,
  rED,
  T0W, // 0 water??
  TIW, // 1. water
  T0R,
  numF, // Water filling number
  unbF, // unbalance factor
  unbC, // unbalance compensation
  NtcW, // NTW Wash value
  NtcD, // NTW Dry value
  motS, // motor speed value
  APSoff, // APS offset
  APSfreq, // APS frequency
  chartL // chart line
```
